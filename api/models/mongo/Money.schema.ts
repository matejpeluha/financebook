import {Model, Schema} from "mongoose";
import {connection} from "../../config/mongo.config";
import * as autoIncrement from "mongoose-auto-increment";

interface MoneyInterface {
    userId: number;
    totalMoney: number;
    futureMoney: number[];
    pastMoney: number[];
}


const MoneySchema: Schema<MoneyInterface> = new Schema<MoneyInterface>({
    userId: {type: Number, ref: 'User', required: true},
    totalMoney: {type: Number, required: true},
    futureMoney: [{type: Number, ref: 'MoneyChange', required: false}],
    pastMoney: [{type: Number, ref: 'MoneyChange', required: false}]
});

MoneySchema.plugin(autoIncrement.plugin, 'Money');

const MoneyModel: Model<MoneyInterface> = connection.model<MoneyInterface>('Money', MoneySchema);

export {MoneyModel, MoneyInterface};