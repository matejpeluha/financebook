import {Model, Schema} from "mongoose";
import {connection} from "../../config/mongo.config";
import * as autoIncrement from "mongoose-auto-increment";


interface UserInterface {
    name: string;
    mail: string;
    password: string;
}

interface ExtendedUserInterface extends UserInterface {
    _id: number;
}

const UserSchema: Schema<UserInterface> = new Schema<UserInterface>({
    name: {type: String, required: true},
    mail: {type: String, required: true, unique: true},
    password: {type: String, required: true}
});

UserSchema.plugin(autoIncrement.plugin, 'User');

const UserModel: Model<UserInterface> = connection.model<UserInterface>('User', UserSchema);

export {UserModel, UserInterface, ExtendedUserInterface};