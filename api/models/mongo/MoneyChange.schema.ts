import {Model, Schema} from "mongoose";
import {connection} from "../../config/mongo.config";
import * as autoIncrement from "mongoose-auto-increment";



enum MoneyChangeType{
    MINUS = 'MINUS',
    PLUS = 'PLUS'
}

interface MoneyChangeInterface{
    changeType: MoneyChangeType;
    text: string;
    money: number;
    date: Date | undefined;
}

interface ExtendedMoneyChangeInterface extends MoneyChangeInterface{
    _id: number;
}

const MoneyChangeSchema: Schema<MoneyChangeInterface> = new Schema<MoneyChangeInterface>({
    changeType: {type: String, enum: ['MINUS', 'PLUS'], required: true},
    text: {type: String, required: true},
    money: {type: Number, required: true},
    date: {type: Date, required: false}
});

MoneyChangeSchema.plugin(autoIncrement.plugin, 'MoneyChange');

const MoneyChangeModel: Model<MoneyChangeInterface> = connection.model<MoneyChangeInterface>('MoneyChange', MoneyChangeSchema);

export {MoneyChangeModel, MoneyChangeInterface, ExtendedMoneyChangeInterface};