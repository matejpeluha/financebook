interface CookieSessionOptions{
    httpOnly: boolean,
    sameSite: boolean | "none",
    maxAge: number,
}

export {CookieSessionOptions};