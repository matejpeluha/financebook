import {CookieSessionOptions} from "./CookieSessionOptions";
import MongoStore from "connect-mongo";


interface SessionOptions{
    secret: string;
    store: MongoStore;
    name: string;
    saveUninitialized: boolean;
    cookie: CookieSessionOptions;
    resave: boolean;
}

export {SessionOptions};