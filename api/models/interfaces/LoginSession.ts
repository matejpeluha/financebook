import { Session } from "express-session";

interface LoginSession extends Session {
    userId: number | undefined;
}

export {LoginSession};