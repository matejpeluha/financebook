import {Router} from "express";
import {registration} from "./registration";
import {login} from "./login";
import {getRequest} from "./get.request";
import {logout} from "./logout";
import {money} from "./money";

const root: Router = Router();

root.get("/", getRequest);

root.use("/registration", registration);
root.use("/login", login);
root.use("/logout", logout);
root.use("/money", money);

export {root};