import {Request, Response} from "express";
import {MoneyAdderService} from "../../../../services/MoneyAdder.service";

const postRequest = (req: Request, res: Response): void => {
    const moneyAdderService: MoneyAdderService = new MoneyAdderService(req, res);
    moneyAdderService.addMoney("past");
}

export {postRequest};