import {Router} from "express";

import {putRequest} from "./put.request";
import {deleteParamRequest} from "./delete.param.request";
import {postRequest} from "./post.request";

const past: Router = Router();


past.post("/", postRequest);
past.put("/:id", putRequest);
past.delete("/:id", deleteParamRequest);


export {past};