import {Router} from "express";
import {getRequest} from "./get.request";
import {future} from "./future";
import {past} from "./past";
import {putRequest} from "./put.request";

const money: Router = Router();

money.get("/", getRequest);
money.put("/", putRequest);

money.use("/future", future);
money.use("/past", past);

export {money};