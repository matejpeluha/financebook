import {Request, Response} from "express";
import {MoneyEditorService} from "../../../services/MoneyEditor.service";

const putRequest = (req: Request, res: Response): void => {
    const totalMoney: number = req.body.totalMoney;
    const moneyEditorService: MoneyEditorService = new MoneyEditorService(req, res);
    moneyEditorService.editTotalMoney(totalMoney);
}

export {putRequest};