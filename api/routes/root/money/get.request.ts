import {Request, Response} from "express";
import {MoneyGetterService} from "../../../services/MoneyGetter.service";

const getRequest = (req: Request, res: Response): void => {
    const moneyGetterService: MoneyGetterService = new MoneyGetterService(req, res);
    moneyGetterService.getMoneyInfo();
}

export {getRequest};