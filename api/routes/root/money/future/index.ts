import {Router} from "express";
import {postRequest} from "./post.request";
import {putRequest} from "./put.request";
import {deleteParamRequest} from "./delete.param.request";

const future: Router = Router();


future.post("/", postRequest);
future.put("/:id", putRequest);
future.delete("/:id", deleteParamRequest);


export {future};