import {Request, Response} from "express";
import {MoneyEditorService} from "../../../../services/MoneyEditor.service";

const putRequest = (req: Request, res: Response): void => {
    const moneyEditorService: MoneyEditorService = new MoneyEditorService(req, res);
    moneyEditorService.editMoney();
}

export {putRequest};