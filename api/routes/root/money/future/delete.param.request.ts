import {Request, Response} from "express";
import {MoneyDeleterService} from "../../../../services/MoneyDeleter.service";

const deleteParamRequest = (req: Request, res: Response): void => {
    const moneyDeleterService: MoneyDeleterService = new MoneyDeleterService(req, res);
    moneyDeleterService.deleteMoney("future");
}

export {deleteParamRequest};