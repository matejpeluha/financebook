import {Request, Response} from "express";
import {UserLoginService} from "../../services/UserLogin.service";

const getRequest = (req: Request, res: Response): void => {
    const userLoginService: UserLoginService =  new UserLoginService(req, res);
    userLoginService.checkLogged();
}

export {getRequest};
