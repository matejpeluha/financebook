import {Router} from "express";
import {postRequest} from "./post.request";
import {getRequest} from "./get.request";

const login: Router = Router();

login.get("/", getRequest);
login.post("/", postRequest);

export {login};