import { Router } from "express";
import {getRequest} from "./get.request";


const logout: Router = Router();

logout.get("/", getRequest);

export {logout};