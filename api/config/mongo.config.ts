import mongoose, {Connection} from "mongoose";
import * as autoIncrement from 'mongoose-auto-increment';
import MongoStore from "connect-mongo";
import * as dotenv from "dotenv";

dotenv.config({path: __dirname + '/.env'})
let uri: string = "";
if(process.env.MONGO_DB_URI){
    uri = process.env.MONGO_DB_URI;
}

const connection: Connection = mongoose.createConnection(uri);
autoIncrement.initialize(connection);


const oneDay: number = 1000 * 60 * 60 * 24;
const mongoStore: MongoStore = MongoStore.create({
    mongoUrl: uri,
    ttl: oneDay,
    autoRemove: "native",
    crypto: {
        secret: "Mongo-Store-Secret-752648-fgfd486gfd-4"
    }
});


export {connection, mongoStore};