import {RequestHandler} from "express";
import session from "express-session";
import {CookieSessionOptions} from "../models/interfaces/options/CookieSessionOptions";
import {SessionOptions} from "../models/interfaces/options/SessionOptions";
import {mongoStore} from "./mongo.config";


const oneDay: number = 1000 * 60 * 60 * 24;

const cookieSessionOptions: CookieSessionOptions = {
    httpOnly: true,
    sameSite: true,
    maxAge: oneDay,
};

const sessionOptions: SessionOptions = {
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir768",
    store: mongoStore,
    name: "financebook-login",
    saveUninitialized: true,
    cookie: cookieSessionOptions,
    resave: true
};

const loginSession: RequestHandler = session(sessionOptions);

export {loginSession};
