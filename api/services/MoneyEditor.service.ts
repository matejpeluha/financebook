import {Request, Response} from "express";
import {LoginSession} from "../models/interfaces/LoginSession";
import {MoneyChangeInterface, MoneyChangeModel} from "../models/mongo/MoneyChange.schema";
import {MoneyInterface, MoneyModel} from "../models/mongo/Money.schema";
import {CallbackError} from "mongoose";

class MoneyEditorService{
    private res: Response;
    private loginSession: LoginSession;
    private readonly moneyChange: MoneyChangeInterface;
    private readonly moneyChangeId: number;


    constructor(req: Request, res: Response) {
        this.res = res;
        this.loginSession = (req.session as LoginSession);
        this.moneyChange = req.body;
        this.moneyChangeId = parseInt(req.params.id, 10);
    }

    public editMoney(): void{
        if(this.loginSession.userId !== undefined){
            this.checkUsersMoney()
        }
        else{
            this.res.status(401).send("User not logged");
        }
    }

    private checkUsersMoney(): void{
        const filter: {userId: number | undefined} = {
            userId: this.loginSession.userId
        };

        MoneyModel.findOne(filter, this.findUserMoney);
    }

    private findUserMoney = (err: CallbackError, money: MoneyInterface): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!money){
            this.res.status(404).send("Money change was deleted or corrupted");
        }
        else if (!money.futureMoney.includes(this.moneyChangeId) && !money.pastMoney.includes(this.moneyChangeId)){
            this.res.status(401).send("This money change does not belongs to logged user");
        }
        else{
            MoneyChangeModel.findByIdAndUpdate(this.moneyChangeId, this.moneyChange, this.editMoneyChange);
        }
    }

    private editMoneyChange = (err: CallbackError, moneyChange: any): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!moneyChange){
            this.res.status(404).send("Money change was deleted or corrupted");
        }
        else{
            this.res.sendStatus(200);
        }
    }

    public editTotalMoney(totalMoney: number): void{
        if(this.loginSession.userId !== undefined){
            this.changeTotalMoney(totalMoney);
        }
        else{
            this.res.status(401).send("User not logged");
        }
    }

    private changeTotalMoney(totalMoney: number): void{
        const filter: {userId: number | undefined} = {
            userId: this.loginSession.userId
        };

        const update: {totalMoney: number} = {
            totalMoney: totalMoney
        };

        const options: {} = {};

        MoneyModel.findOneAndUpdate(filter, update, options, this.updateMoney);
    }

    private updateMoney = (err: CallbackError, money: any): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!money){
            this.res.status(404).send("User was deleted or corrupted");
        }
        else{
            this.res.sendStatus(200);
        }
    }


}

export {MoneyEditorService};