import {Request, Response} from "express";
import {LoginInfo} from "../models/interfaces/LoginInfo";
import {ExtendedUserInterface, UserModel} from "../models/mongo/User.schema";
import {CallbackError} from "mongoose";
import bcrypt from "bcrypt";
import {LoginSession} from "../models/interfaces/LoginSession";


class UserLoginService{
    private res: Response;
    private loginSession: LoginSession;
    private loginInfo: LoginInfo;
    private user: ExtendedUserInterface | undefined;


    constructor(req: Request, res: Response) {
        this.res = res;
        this.loginSession = (req.session as LoginSession);
        this.loginInfo = req.body;
    }

    public loginUser(): void{
        const filter: {mail: string} = {
            mail: this.loginInfo.mail
        }

        UserModel.findOne(filter, this.findUserByMail);
    }

    private findUserByMail = (err: CallbackError, user: ExtendedUserInterface | undefined): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!user){
            this.res.status(404).send("User does not exists");
        }
        else{
            this.user = user;
            bcrypt.compare(this.loginInfo.password, user.password, this.comparePasswords);
        }
    }

    private comparePasswords = (err: Error | undefined, result: boolean | undefined): void => {

        if (err){
            this.res.status(500).send(err);
        }
        else if (!result){
            this.res.status(401).send("Wrong password");
        }
        else{
            this.loginSession.userId = this.user?._id;
            this.res.status(200).send("Logged IN");
        }
    }

    public checkLogged(): void{
        if(this.loginSession.userId !== undefined){
            this.showLoggedUser();
        }
        else{
            this.res.status(401).send("NOT logged");
        }
    }

    private showLoggedUser(): void{
        const filter: {_id: number | undefined} = {
            _id: this.loginSession.userId
        };

        UserModel.findOne(filter, this.findUserById);
    }

    private findUserById = (err: CallbackError, user: ExtendedUserInterface | undefined): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!user){
            this.res.status(401).send("User was deleted or is corrupted");
        }
        else{
            this.res.status(200).send(user.name + " logged IN");
        }
    }

    public logoutUser(): void{
        this.loginSession.destroy(() => {
            this.res.status(200).send("Logged OUT");
        })
    }
}

export {UserLoginService};