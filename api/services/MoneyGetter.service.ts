import {Request, Response} from "express";
import {LoginSession} from "../models/interfaces/LoginSession";
import {MoneyInterface, MoneyModel} from "../models/mongo/Money.schema";
import {CallbackError} from "mongoose";

class MoneyGetterService{
    private res: Response;
    private loginSession: LoginSession;


    constructor(req: Request, res: Response) {
        this.res = res;
        this.loginSession = (req.session as LoginSession);
    }

    public getMoneyInfo(){
        if(this.loginSession.userId == undefined){
            this.res.status(404).send("Not logged user");
        }
        else{
            const filter: {userId: number} = {
                userId: this.loginSession.userId
            };

            MoneyModel.findOne(filter, this.findMoneyInfo).populate('userId').populate('pastMoney').populate('futureMoney');
        }
    }

    private findMoneyInfo = (err: CallbackError, money: any): void => {
        if(err){
            this.res.status(500).send(err);
        }
        else if (!money){
            this.res.status(404).send("Info about money not found");
        }
        else{
            const json = {
                name: money.userId.name,
                totalMoney: money.totalMoney,
                futureMoney: money.futureMoney,
                pastMoney: money.pastMoney
            }
            this.res.json(json);
        }
    }


}

export {MoneyGetterService};