import {Request, Response} from "express";
import {ExtendedUserInterface, UserInterface, UserModel} from "../models/mongo/User.schema";
import bcrypt from "bcrypt";
import {CallbackError, EnforceDocument } from "mongoose";
import {MoneyInterface, MoneyModel} from "../models/mongo/Money.schema";


class UserRegistrationService{
    private readonly user: UserInterface;
    private res: Response;

    constructor(req: Request, res: Response) {
        this.res = res;
        this.user = req.body;
    }
    
    public registerUser(): void{
        const saltRounds: number = 10;
        bcrypt.hash(this.user.password, saltRounds, this.convertUserToHashedPassword);
    }

    private convertUserToHashedPassword = (err: Error | undefined, hashedPassword: string): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else{
            this.user.password = hashedPassword;
            this.saveUser();
        }
    }

    private saveUser(): void{
        const userModel: EnforceDocument<UserInterface, {}> = new UserModel(this.user);
        userModel.save(this.finishSaveUser);
    }

    private finishSaveUser = (err: CallbackError, user: UserInterface) => {
        if (err){
            this.res.status(500).send(err);
        }
        else{
            const extendedUser: ExtendedUserInterface = (user as ExtendedUserInterface);
            const money: MoneyInterface = {
                userId: extendedUser._id,
                totalMoney: 0,
                futureMoney: [],
                pastMoney: []
            }

            const moneyModel: EnforceDocument<MoneyInterface, {}> = new MoneyModel(money);
            moneyModel.save(this.finishSaveMoneyModel);
        }
    }

    private finishSaveMoneyModel = (err: CallbackError, money: MoneyInterface) => {
        if (err){
            this.res.status(500).send(err);
        }
        else{
            this.res.sendStatus(200);
        }
    }
}

export {UserRegistrationService};