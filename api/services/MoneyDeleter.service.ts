import {Request, Response} from "express";
import {LoginSession} from "../models/interfaces/LoginSession";
import {MoneyChangeModel} from "../models/mongo/MoneyChange.schema";
import {MoneyModel} from "../models/mongo/Money.schema";
import {CallbackError} from "mongoose";


class MoneyDeleterService {
    private res: Response;
    private loginSession: LoginSession;
    private readonly moneyChangeId: number;
    private moneyTime: string | undefined;


    constructor(req: Request, res: Response) {
        this.res = res;
        this.loginSession = (req.session as LoginSession);
        this.moneyChangeId = parseInt(req.params.id, 10);
    }

    public deleteMoney(moneyTime: string): void{
       if(this.loginSession.userId !== undefined){
           this.moneyTime = moneyTime;
           this.checkUsersMoney()
       }
       else{
           this.res.status(401).send("User not logged");
       }
    }

    private checkUsersMoney(): void{
        const filter: {userId: number | undefined} = {
            userId: this.loginSession.userId
        };

        const update: { "$pull": any } = this.getUpdate();

        const options: {returnOriginal: boolean} = {
            returnOriginal: true
        }

        MoneyModel.findOneAndUpdate(filter, update, options, this.findUser);
    }

    private getUpdate(): { "$pull": any }{
        let update;
        if(this.moneyTime == "future"){
            update = {
                "$pull": {
                    "futureMoney": this.moneyChangeId
                }
            };
        }
        else{
            update = {
                "$pull": {
                    "pastMoney": this.moneyChangeId
                }
            };
        }

        return update;
    }

    private findUser = (err: CallbackError, money: any): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!money){
            this.res.status(404).send("User was deleted or corrupted");
        }
        else if (!money.futureMoney.includes(this.moneyChangeId) && !money.pastMoney.includes(this.moneyChangeId)){
            this.res.status(401).send("This money change does not belongs to logged user");
        }
        else{
            this.deleteMoneyFromUser()
        }
    }

    private deleteMoneyFromUser(): void{
        const filter: {_id: number} = {
            _id: this.moneyChangeId
        }

        const options: {} = {};

        MoneyChangeModel.findOneAndDelete(filter, options, this.deleteMoneyChange);
    }

    private deleteMoneyChange = (err: CallbackError, moneyChange: any): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!moneyChange){
            this.res.status(404).send("Money change does not exists");
        }
        else{
            this.res.sendStatus(200);
        }
    }
}

export {MoneyDeleterService}