import {Request, Response} from "express";
import {LoginSession} from "../models/interfaces/LoginSession";
import {ExtendedMoneyChangeInterface, MoneyChangeInterface, MoneyChangeModel} from "../models/mongo/MoneyChange.schema";
import {MoneyModel} from "../models/mongo/Money.schema";
import {CallbackError, EnforceDocument} from "mongoose";

class MoneyAdderService{
    private res: Response;
    private loginSession: LoginSession;
    private readonly moneyChange: MoneyChangeInterface;
    private moneyTime: string | undefined;

    constructor(req: Request, res: Response) {
        this.res = res;
        this.loginSession = (req.session as LoginSession);
        this.moneyChange = req.body;
    }

    public addMoney(moneyTime: string): void{
        if(this.loginSession.userId == undefined){
            this.res.status(401).send("Not logged user");
        }
        else{
            this.moneyTime = moneyTime;
            const moneyChangeModel: EnforceDocument<MoneyChangeInterface, {}> = new MoneyChangeModel(this.moneyChange);
            moneyChangeModel.save(this.saveToMoneyModel);
        }
    }

    private saveToMoneyModel = (err: CallbackError, moneyChange: MoneyChangeInterface): void => {
        if(err){
            this.res.status(500).send(err);
        }
        else{
            const filter: {userId: number | undefined} = {
                userId: this.loginSession.userId
            };

            const moneyChangeId: number = (moneyChange as ExtendedMoneyChangeInterface)._id;
            const update = this.getUpdate(moneyChangeId)

            const options: {} = {};

            MoneyModel.findOneAndUpdate(filter, update, options, this.finishSaveMoneyChange);
        }
    }

    private getUpdate(moneyChangeId: number): { "$push": any }{
        let update;
        if(this.moneyTime == "future"){
            update = {
                "$push": {
                    "futureMoney": moneyChangeId
                }
            };
        }
        else{
            update = {
                "$push": {
                    "pastMoney": moneyChangeId
                }
            };
        }

        return update;
    }

    private finishSaveMoneyChange = (err: CallbackError, money: any): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (money == null){
            this.res.status(404).send("Money info not found");
        }
        else{
            this.res.sendStatus(200);
        }
    }


}

export {MoneyAdderService};