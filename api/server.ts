import bodyParser from 'body-parser';
import cors from 'cors';
import express, {Application, RequestHandler} from 'express';

import {apiRouter} from './router';
import {loginSession} from "./config/session.config";
import cookieParser from "cookie-parser";
import path from "path";

//deklaracia expressu a portu
const app: Application = express();
const port: string | number = process.env.PORT || 5000;

//CORS POLICIES
app.use(cors());

//KONFIGURACIA BODY PARSER MIDDLEWARU NA PARSOVANIE TELA REQUESTU
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/*
SESSION
 */
app.use(cookieParser());
app.use(loginSession);

// ADD MIDDLEWARE TO SERVE ALL OUR STATIC BUILD FILES.
app.use(express.static(path.join(__dirname, '..', 'client', 'build')))

/*
REQUESTY
 */
app.use(apiRouter);

/*
SPUSTENIE APPKY
 */
app.listen(port, () => {
    console.log("APP running on port " + port);
})
