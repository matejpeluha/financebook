import './App.css';
import React, {Component} from "react";
import LoginPage from "./components/loginPage";
import UserPage from "./components/userPage";
import {apiPath} from "./api-path";


class App extends Component{
   state = {
       isLogged: undefined
   }

   componentDidMount() {
       let reqHeader = new Headers();
       reqHeader.append('Content-Type', 'application/json');

       let initObject = {
           method: 'GET', headers: reqHeader
       };

       const userRequest = new Request(apiPath + '/api/login', initObject);

       fetch(userRequest)
           .then(this.handleResponse);
   }

    handleResponse = (response) =>{
        if (response.status === 401){
            this.setState({isLogged: false});
        }
        else if(response.status === 200){
            this.setState({isLogged: true});
        }
    }


    render(){
       if(this.state.isLogged === true){
           return <UserPage onLoggedChange={this.handleLoggedChange}/>
       }
       else if(this.state.isLogged === false){
           return <LoginPage onLoggedChange={this.handleLoggedChange}/>
       }
       else{
           return "";
       }
   }

   handleLoggedChange = (isLogged) => {
       this.setState({isLogged: isLogged});
   }

}



export default App;
