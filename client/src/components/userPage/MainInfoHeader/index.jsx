import React, {Component} from "react";

import "./index.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {apiPath} from "../../../api-path";

class MainInfoHeader extends Component {
    state = {
        isEditing: false
    }

    render() {
        return (
            <h1 id="money-info-header">
                {this.props.name + ": "}{this.getNumberElement()}{"€"}
                <br/>
                <span id="money-info-edit" onClick={this.handleEditMainMoney} >
                    <FontAwesomeIcon icon={faEdit} size="xs"/>
                </span>
            </h1>
        );
    }

    getNumberElement(){
        if(this.state.isEditing){
            return <input id="main-money-input" type="number" defaultValue={this.props.money} onKeyDown={this.handleOnKeyDown}/>
        }
        else{
            return this.props.money;
        }
    }

    handleOnKeyDown = (event) =>{
        if (event.key === "Enter"){
            this.saveMainMoney();
        }
    }

    handleEditMainMoney = () => {
        if(this.state.isEditing) {
            this.saveMainMoney();
        }
        else{
            this.setState({isEditing: true});
        }
    }

    saveMainMoney(){
        this.props.onChangeLoading(true);
        const json = this.getJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'PUT', headers: reqHeader, body: json
        };

        const userRequest = new Request(apiPath + '/api/money', initObject);

        fetch(userRequest)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    getJson() {
        const totalMoney= document.getElementById("main-money-input").value;
        const json = {
            totalMoney: totalMoney,
        };

        return JSON.stringify(json);
    }

    handleResponse = (response) => {
        if (response.ok){
            this.props.onChangeMainMoney();
            this.setState({isEditing: false});
        }
    }

    handleError = (err) => {
        console.log(err);
    }
}

export default MainInfoHeader;