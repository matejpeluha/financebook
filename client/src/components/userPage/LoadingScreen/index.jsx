import React, {Component} from "react";

import "./index.css";

class LoadingScreen extends Component {
    render() {
        if(this.props.show) {
            return (
                <div id="loading-screen">
                    <div className="loadingio-spinner-spinner-xdbije6hhyq">
                        <div className="ldio-kh9z6ia5348">
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                            <div/>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return "";
        }
    }
}

export default LoadingScreen;