import React, {Component} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPowerOff} from "@fortawesome/free-solid-svg-icons";

import "./index.css";
import {apiPath} from "../../api-path";
import MainInfoHeader from "./MainInfoHeader";
import MoneyChangeAdder from "./MoneyChangeAdder";
import MoneyChange from "./MoneyChange";
import LoadingScreen from "./LoadingScreen";

class UserPage extends Component {
    state = {
        name: "",
        totalMoney: 0,
        futureMoney: [],
        pastMoney: [],
        showLoading: false
    }

    render() {
        return (
            <main>
                <LoadingScreen show={this.state.showLoading}/>
                <div id="page-top">
                    <span id="logout" onClick={this.logout}>
                        <FontAwesomeIcon icon={faPowerOff} size="lg"/>
                    </span>
                </div>
                <MainInfoHeader
                    name={this.state.name}
                    money={this.state.totalMoney}
                    onChangeMainMoney={this.loadMoneyInfo}
                    onChangeLoading={this.changeLoading}
                />
                <div id="underline"/>
                <section>
                    {this.state.futureMoney.map(this.getMoneyChange)}
                </section>
                <MoneyChangeAdder
                    onMoneyChangeAdded={this.loadMoneyInfo}
                    onChangeLoading={this.changeLoading}
                />
            </main>
        );
    }

    componentDidMount() {
        this.loadMoneyInfo();
    }

    loadMoneyInfo = () => {
        const reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request(apiPath + '/api/money', initObject);

        fetch(userRequest)
            .then(response => {return response.json();})
            .then(this.handleLoadMoney)
            .catch(this.handleError);
    }

    handleLoadMoney = (json) => {
        json.showLoading = false;
        this.setState(json);
    }

    logout = () => {
        const reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request(apiPath + '/api/logout', initObject);

        fetch(userRequest)
            .then(this.handleLogoutResponse)
            .catch(this.handleError);
    }

    handleLogoutResponse = (response) => {
        if(response.ok){
            this.props.onLoggedChange(false);
        }
    }

    handleError = (err) => {
        console.log(err);
    }

    getMoneyChange = (moneyChange, key) => {
        return (
            <MoneyChange
                key={key}
                moneyChange={moneyChange}
                totalMoney={this.state.totalMoney}
                onMoneyChange={this.loadMoneyInfo}
                onChangeLoading={this.changeLoading}
            />
        );
    }

    changeLoading = (showLoading) => {
        this.setState({showLoading: showLoading});
    }
}

export default UserPage;