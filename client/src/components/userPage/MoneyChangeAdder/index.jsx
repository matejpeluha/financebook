import React, {Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare, faSave, faTrashAlt} from "@fortawesome/free-regular-svg-icons";

import "./index.css";
import {apiPath} from "../../../api-path";

class MoneyChangeAdder extends Component {
    state = {
        isAdding: false
    }

    render() {
        return (
            <div id="money-change-adder">
                {this.getAdder()}
            </div>
        );
    }

    getAdder(){
        if(this.state.isAdding){
            return (
                <div id="new-money-change">
                    <div id="new-money-change-container">
                        <div id="new-money-input-container">
                            <input id="subject-input" type="text" placeholder="Subject"/>
                            <input id="money-input" type="number" placeholder="Money"/>
                        </div>
                        <br/>
                        <div>
                            <span id="save-container" onClick={this.saveMoneyChange}>
                                <FontAwesomeIcon icon={faSave} size={"1x"}/>
                            </span>
                            <span id="remove-container" onClick={() => this.changeAdding(false)}>
                                <FontAwesomeIcon icon={faTrashAlt} size={"1x"}/>
                            </span>
                        </div>
                    </div>
                    <div id="message">
                        Subject and Money have to be filled!
                    </div>
                </div>
            );
        }
        else{
            return (
                <span id="icon-container" onClick={() => this.changeAdding(true)}>
                    <FontAwesomeIcon icon={faPlusSquare} size={"2x"}/>
                </span>
            );
        }
    }

    changeAdding = (isAdding) => {
        this.setState({isAdding: isAdding});
    }

    saveMoneyChange = () => {
        this.props.onChangeLoading(true);
        const json = this.getJson();
        if(!json){
            document.getElementById("message").style.opacity = "1";
        }
        else{
            document.getElementById("message").style.opacity = "0";
            this.fetchApi(json);
        }
    }

    getJson(){
        let changeType;
        const subject = document.getElementById("subject-input").value;
        const money = document.getElementById("money-input").value;
        if(subject === "" || money === ""){
            return undefined;
        }

        if(money < 0){
            changeType = "MINUS";
        }
        else{
            changeType = "PLUS";
        }

        const json = {
            changeType: changeType,
            text: subject,
            money: money
        }

        return JSON.stringify(json);
    }

    fetchApi(json){
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: json
        };

        const userRequest = new Request(apiPath + '/api/money/future', initObject);

        fetch(userRequest)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    handleResponse = (response) => {
        if(response.ok){
            this.setState({isAdding: false});
            this.props.onMoneyChangeAdded();
        }
    }

    handleError = (err) => {
        console.log(err);
    }
}

export default MoneyChangeAdder;