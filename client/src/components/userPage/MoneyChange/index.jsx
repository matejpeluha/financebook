import React, {Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle, faEdit, faTrashAlt, faSave} from "@fortawesome/free-regular-svg-icons";
import {faLongArrowAltDown, faLongArrowAltUp} from "@fortawesome/free-solid-svg-icons";

import "./index.css";
import {apiPath} from "../../../api-path";

class MoneyChange extends Component {
    state = {
        isEditing: false
    }

    render() {
        return (
            <div id="money-change">
                {this.getInfo()}
                {this.getIcons()}
            </div>
        );
    }

    getInfo(){
        if(this.state.isEditing){
            return (
                <div id="info-container">
                    <span>
                        <input id="old-subject-input" type="text" placeholder="Subject" defaultValue={this.props.moneyChange.text} onKeyDown={this.onKeyDownEdit}/>
                    </span>
                    <span>
                        <input id="old-money-input" type="number" placeholder="Money" defaultValue={this.props.moneyChange.money} onKeyDown={this.onKeyDownEdit}/>
                    </span>
                </div>
            );
        }
        else{
            return (
                <div id="info-container">
                    <span>{this.getArrowIcon()}{this.props.moneyChange.text}</span>
                    <span>{this.getMoneyPlus()}{this.props.moneyChange.money}€</span>
                </div>
            );
        }
    }

    getIcons(){
        if(this.state.isEditing){
            return (
                <div id="faIcon-container">
                    <span id="faSave-container" onClick={this.finishEditing}><FontAwesomeIcon icon={faSave} /></span>
                </div>
            );
        }
        else{
            return (
                <div id="faIcon-container">
                    <span id="faCheck-container" onClick={this.migrateMoney}><FontAwesomeIcon icon={faCheckCircle} /></span>
                    <span id="faEdit-container" onClick={this.startEditing}><FontAwesomeIcon icon={faEdit} /></span>
                    <span id="faTrash-container" onClick={this.deleteMoneyChange}><FontAwesomeIcon icon={faTrashAlt} /></span>
                </div>
            );
        }

    }

    getArrowIcon(){
        if(this.props.moneyChange.changeType === "MINUS"){
            return <span className="arrow-down"><FontAwesomeIcon icon={faLongArrowAltDown}/></span>;
        }
        else{
            return <span className="arrow-up"><FontAwesomeIcon icon={faLongArrowAltUp}/></span>;
        }
    }

    getMoneyPlus(){
        if(this.props.moneyChange.changeType === "PLUS"){
            return "+";
        }
        else{
            return "";
        }
    }

    deleteMoneyChange= () => {
        this.props.onChangeLoading(true);
        const reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'DELETE', headers: reqHeader
        };

        const url = apiPath + '/api/money/future/' + this.props.moneyChange._id;
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.handleDeleteResponse)
            .catch(this.handleError);
    }

    handleDeleteResponse = (response) => {
        if (response.ok){
            this.props.onMoneyChange();
        }
    }

    migrateMoney = () => {
        this.props.onChangeLoading(true);
        const json = this.getMigrateJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'PUT', headers: reqHeader, body: json
        };

        const userRequest = new Request(apiPath + '/api/money', initObject);

        fetch(userRequest)
            .then(this.handleMigrateResponse)
            .catch(this.handleError);
    }

    getMigrateJson(){
        const newTotalMoney = this.props.totalMoney + this.props.moneyChange.money;
        const json = {
            totalMoney: newTotalMoney
        }

        return JSON.stringify(json);
    }

    handleMigrateResponse = (response) => {
        if(response.ok){
            this.deleteMoneyChange();
        }
    }

    handleError = (err) => {
        console.log(err);
    }

    startEditing = () => {
        this.setState({isEditing: true});
    }

    onKeyDownEdit = (event) => {
        if(event.key === "Enter"){
            this.finishEditing();
        }
    }

    finishEditing = () => {
        const subjectInput = document.getElementById("old-subject-input");
        const moneyInput = document.getElementById("old-money-input");
        const subject = subjectInput.value;
        const money = moneyInput.value;

        if(!subject){
            subjectInput.className = "wrong-input"
        }
        else{
            subjectInput.className = ""
        }

        if(!money){
            moneyInput.className = "wrong-input"
        }
        else{
            moneyInput.className = ""
        }

        if(money && subject){
            this.fetchEdit();
        }
    }

    fetchEdit(){
        this.props.onChangeLoading(true);
        const json = this.getEditJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'PUT', headers: reqHeader, body: json
        };

        const url = apiPath + '/api/money/past/' + this.props.moneyChange._id;
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.handleEditResponse)
            .catch(this.handleError);
    }

    getEditJson(){
        let changeType;
        const money = document.getElementById("old-money-input").value;
        const subject = document.getElementById("old-subject-input").value;

        if(money < 0){
            changeType = "MINUS";
        }
        else{
            changeType = "PLUS";
        }

        const json = {
            changeType: changeType,
            money: money,
            text: subject
        }

        return JSON.stringify(json);
    }

    handleEditResponse = (response) => {
        if(response.ok){
            this.setState({isEditing: false}, () => this.props.onMoneyChange());
        }
    }
}

export default MoneyChange;