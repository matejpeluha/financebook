import React, {Component} from "react";

import "./index.css";
import {apiPath} from "../../../api-path";

class LoginForm extends Component {
    render() {
        return (
            <form id="login-form" onSubmit={this.handleSubmit}>
                <input id="login-mail-input" type="email" placeholder="e-Mail" required />
                <input id="login-password-input" type="password" placeholder="Password" required/>
                <p id="message">Mail is not registered</p>
                <button>Submit</button>
            </form>
        );
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const json = this.getRegistrationJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: json
        };

        const userRequest = new Request(apiPath + '/api/login', initObject);

        fetch(userRequest)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    getRegistrationJson() {
        const mail = document.getElementById("login-mail-input").value;
        const password = document.getElementById("login-password-input").value;

        const json = {
            mail: mail,
            password: password
        };

        return JSON.stringify(json);
    }

    handleResponse = (response) => {
        if (response.ok) {
            document.getElementById("login-mail-input").value = "";
            document.getElementById("login-password-input").value = "";
            document.getElementById("message").style.opacity = "0";
            this.props.onLoggedChange(true);
        }
        else if(response.status === 404) {
            document.getElementById("message").style.opacity = "1";
            document.getElementById("message").innerText = "Mail is not registered";
        }
        else if(response.status === 401){
            document.getElementById("message").style.opacity = "1";
            document.getElementById("message").innerText = "Wrong password";
        }
    }

    handleError = (err) => {
        console.log(err);
    }
}

export default LoginForm;