import React, {Component} from "react";

import "./index.css";

class FormHeader extends Component {
    render() {
        return (
            <div id="form-chooser">
                <div>
                    <p className={this.getLoginView()} onClick={() => this.props.onFormChange("login")}>
                        Login
                    </p>
                </div>
                <div>
                    <p className={this.getRegistrationView()} onClick={() => this.props.onFormChange("registration")}>
                        Registration
                    </p>
                </div>
            </div>
        );
    }

    getLoginView(){
        if (this.props.show === "login"){
            return "active";
        }
    }

    getRegistrationView(){
        if (this.props.show === "registration"){
            return "active";
        }
    }
}

export default FormHeader;