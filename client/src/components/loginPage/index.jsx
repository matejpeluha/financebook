import React, {Component} from "react";

import "./index.css";
import FormHeader from "./header";
import RegistrationForm from "./registrationForm";
import LoginForm from "./loginForm";

class LoginPage extends Component {
    state = {
        show: "login"
    }


    render() {
        return (
            <main>
                <h2 id="page-header">
                    FinanceBook <span>- handle your future money</span>
                </h2>
                <section id="form-container">
                    <FormHeader show={this.state.show} onFormChange={this.handleOnChange}/>
                    {this.getForm()}
                </section>
            </main>
        );
    }

    handleOnChange = (form) => {
        this.setState({show: form});
    }

    getForm(){
        if(this.state.show === "registration"){
            return <RegistrationForm onFormChange={this.handleOnChange}/>;
        }
        else if (this.state.show === "login"){
            return <LoginForm onLoggedChange={(isLogged) => this.props.onLoggedChange(isLogged)}/>
        }
    }
}

export default LoginPage;