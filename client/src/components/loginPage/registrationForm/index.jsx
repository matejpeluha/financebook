import React, {Component} from "react";

import "./index.css";
import {apiPath} from "../../../api-path";


class RegistrationForm extends Component {
    state = {
        registrationMessage: "",
        registrationColor: ""
    }

    render() {
        return (
            <form id="registration-form" onSubmit={this.handleFormSubmit}>
                <input id="name-input" type="text" placeholder="Name" required/>
                <label>
                    <input id="mail-input" type="email" placeholder="e-Mail" required/>
                    <p id="mail-message">Mail already registered!</p>
                </label>
                <input id="password-input" type="password" placeholder="Password" onChange={this.validatePassword} required/>
                <input id="repeat-password-input" type="password" placeholder="Repeat Password" onChange={this.validatePassword} required/>
                <button type="submit">Submit</button>
            </form>
        );
    }

    validatePassword = () =>{
        const passwordInput = document.getElementById("password-input");
        const repeatPasswordInput = document.getElementById("repeat-password-input");
        const password = passwordInput.value;
        const repeatPassword = repeatPasswordInput.value;

        const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");

        if(!regex.test(password)){
            passwordInput.setCustomValidity("password must be a minimum of 8 characters including number, Upper, Lower And one special characters");
        }
        else if(password !== repeatPassword){
            passwordInput.setCustomValidity("");
            repeatPasswordInput.setCustomValidity("Passwords are not same");
        }
        else{
            repeatPasswordInput.setCustomValidity("");
            passwordInput.setCustomValidity("");
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault();

        const json = this.getRegistrationJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: json
        };

        const userRequest = new Request(apiPath + '/api/registration', initObject);

        fetch(userRequest)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    getRegistrationJson() {
        const name = document.getElementById("name-input").value;
        const mail = document.getElementById("mail-input").value;
        const password = document.getElementById("password-input").value;

        const json = {
            name: name,
            mail: mail,
            password: password
        };

        return JSON.stringify(json);
    }

    handleResponse = (response) => {
        if (response.ok) {
            document.getElementById("name-input").value = "";
            document.getElementById("mail-input").value = "";
            document.getElementById("password-input").value = "";
            document.getElementById("mail-message").style.display = "none";
            this.props.onFormChange("login");
        } else {
            throw new Error(response.status);
        }
    }

    handleError = (err) => {
        document.getElementById("mail-message").style.display = "block";
    }
}


export default RegistrationForm;